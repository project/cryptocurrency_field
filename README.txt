INTRODUCTION
------------

The Cryptocurrency Field is generic Drupal 8 field to store cryptocurrency addresses. The field is configured to a specific currency when added to an entity. User-supplied address are validated to ensure they comply with the currency's formatting specifications before being accepted by Drupal.

The Field currently works with Bitcoin Cash addresses employing the CashAddr format.

REQUIREMENTS
------------

This module currently has no dependencies, however, future functionality will require the BCMath and Hashing PHP extensions.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/node/1897420 for further information.

MAINTAINERS
-----------

Current maintainers:
 * Beakerboy - https://www.drupal.org/u/beakerboy
